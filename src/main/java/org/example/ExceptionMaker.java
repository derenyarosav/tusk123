package org.example;

public class ExceptionMaker {

    public static void main(String[] args) throws SaferException{
        new ExceptionMaker().isThereTwo(getData());
         throw new SaferException("NullPointerException");
    }

    public boolean isThereTwo(Object[] objectsArray) throws SaferException{
        for (Object someObject : objectsArray) {
            // Додати код, що перехоплює вийняток NullPointerException та повертає SaferException
           try {


            if (someObject.equals(2)) {
              return true;

            }
           }catch (NullPointerException e){
               System.out.println(e.getMessage());
           }
        }
        return false;
    }

    public static Object[] getData() {
        return new Object[] { new Object(), null, "test data", 2 };
    }
}

